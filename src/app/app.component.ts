import { Component, OnDestroy, ViewChild } from '@angular/core';
import { AppState } from './app.module';
import { Store } from '@ngrx/store';
import { logout } from './core/auth/store/auth.actions';

@Component({
  selector: 'app-root',
  template: `
    
    <button routerLink="login">login</button>
    <button routerLink="admin" *appIfLogged="'moderator'">admin</button>
    <button routerLink="items" *appIfLogged="'admin'">items</button>
    <button routerLink="pexels-video">pexels-video</button>
    <button routerLink="pexels-image">pexels-image</button>
    <button  *appIfLogged (click)="logoutHandler()">logout</button>
    <hr>
    <router-outlet></router-outlet>
   
  `,
})
export class AppComponent {
  constructor(private store: Store<AppState>) {
  }
  logoutHandler(): void {
    this.store.dispatch(logout())
  }
}
