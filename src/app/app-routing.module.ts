import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuards } from './core/auth/auth.guards';

const routes: Routes = [
  { path: 'items', canActivate: [AuthGuards], loadChildren: () => import('./features/items/items.module').then(m => m.ItemsModule) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'pexels-video', loadChildren: () => import('./features/pexels-video/pexels-video.module').then(m => m.PexelsVideoModule) },
  { path: 'admin', canActivate: [AuthGuards], loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule) },
  { path: 'pexels-image', loadChildren: () => import('./features/pexels-image/pexels-image.module').then(m => m.PexelsImageModule) }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
