import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Item } from '../model/item';

@Component({
  selector: 'app-item-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li 
      *ngFor="let item of items"
      (click)="setActiveItem.emit(item)"
      [style.backgroundColor]="item.id === active?.id ? 'red' : null"
    >
      {{item.name}}
      <button (click)="deleteHandler($event, item)">delete</button>
    </li>
  `,
})
export class ItemListComponent {
  @Input() items: Item[];
  @Input() active: Item;
  @Output() setActiveItem: EventEmitter<Item> = new EventEmitter<Item>();
  @Output() deleteItem: EventEmitter<Item> = new EventEmitter<Item>();

  deleteHandler(event: MouseEvent, item: Item): void {
    event.stopPropagation();
    this.deleteItem.emit(item);
  }
}
