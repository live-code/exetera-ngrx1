import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Item } from '../model/item';
import { NgForm } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Actions, ofType } from '@ngrx/effects';
import { addItemSuccess, cleanActiveItem } from '../store/items.actions';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-item-form',
  template: `
    {{active | json}}
    <form #f="ngForm" (submit)="saveItem.emit(f.value)">
      <input type="text" [ngModel]="active?.name" name="name">
      <button type="submit">{{active ? 'EDIT' : 'ADD'}}</button>
    </form>
  `,
})
export class ItemFormComponent implements OnDestroy {
  @Input() active: Item;
  @Output() saveItem: EventEmitter<Item> = new EventEmitter<Item>();
  @ViewChild('f') form: NgForm;
  destroy$: Subject<void> = new Subject<void>();

  constructor(
    private actions$: Actions,
  ) {
    this.actions$
      .pipe(
        takeUntil(this.destroy$),
        ofType(addItemSuccess, cleanActiveItem)
      )
      .subscribe(() => this.form.reset())
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
