import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemsRoutingModule } from './items-routing.module';
import { ItemsComponent } from './items.component';
import { ItemFormComponent } from './components/item-form.component';
import { ItemListComponent } from './components/item-list.component';
import { FormsModule } from '@angular/forms';
import { itemsReducer, ItemsState } from './store/items.reducer';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './store/items.effects';


export interface UsersFeatureState {
  items: ItemsState;
}
export const reducers: ActionReducerMap<UsersFeatureState> = {
  items: itemsReducer,
};

@NgModule({
  declarations: [
    ItemsComponent,
    ItemFormComponent,
    ItemListComponent
  ],
  imports: [
    CommonModule,
    ItemsRoutingModule,
    FormsModule,
    StoreModule.forFeature('users', reducers),
    EffectsModule.forFeature([ItemsEffects])
  ]
})
export class ItemsModule { }
