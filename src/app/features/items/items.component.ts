import { Component, OnDestroy, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { getItems, getItemsActive, getItemsError, getItemsPending } from './store/items.selectors';
import { cleanActiveItem, deleteItem, loadItems, saveItem, setActiveItem } from './store/items.actions';
import { Item } from './model/item';
import { AppState } from '../../app.module';

@Component({
  selector: 'app-item',
  template: `
    <div *ngIf="error$ | async">Erroraccio!</div>
    <div *ngIf="pending$ | async">Loading...</div>

    <app-item-form [active]="active" (saveItem)="saveHandler($event)"></app-item-form>

    <app-item-list
      [active]="active"
      [items]="items$ | async"
      (setActiveItem)="setActiveHandler($event)"
      (deleteItem)="deleteHandler($event)"
    ></app-item-list>

    <button (click)="cleanActiveHandler()">Clean Active</button>

    <hr>
    <div *ngIf="active">
      L'elemento attivo è:
      {{ active.name }}
    </div>
  `,
})
export class ItemsComponent implements OnDestroy {
  items$: Observable<Item[]> = this.store.pipe(select(getItems));
  error$: Observable<boolean> = this.store.pipe(select(getItemsError));
  pending$: Observable<boolean> = this.store.pipe(select(getItemsPending));
  active$: Observable<Item> = this.store.pipe(select(getItemsActive));
  active: Item;
  sub: Subscription;

  constructor(private store: Store<AppState>) {
    this.sub = this.active$.subscribe(val => this.active = val);
  }

  ngOnInit(): void {
    this.store.dispatch(loadItems());
  }

  saveHandler(item: Item): void {
    this.store.dispatch(saveItem({ item }));
  }

  deleteHandler(item: Item): void {
    this.store.dispatch(deleteItem({ id: item.id }));
  }

  setActiveHandler(item: Item): void {
    this.store.dispatch(setActiveItem({ id: item.id }));
  }

  cleanActiveHandler(): void {
    this.store.dispatch(cleanActiveItem());
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }


}
