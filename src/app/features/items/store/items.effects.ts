import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  addItem,
  addItemFailed,
  addItemSuccess,
  deleteItem, deleteItemFailed,
  deleteItemSuccess, editItem, editItemFailed, editItemSuccess,
  loadItems,
  loadItemsFailed,
  loadItemsSuccess, saveItem
} from './items.actions';
import { catchError, concatMap, map, mapTo, mergeMap, switchMap, switchMapTo, tap, withLatestFrom } from 'rxjs/operators';
import { Item } from '../model/item';
import { forkJoin, of } from 'rxjs';
import { AppState } from '../../../app.module';
import { select, Store } from '@ngrx/store';
import { getItemsActive } from './items.selectors';
import { go } from '../../../core/router/router.actions';

@Injectable()
export class ItemsEffects {


  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(loadItems),
    switchMap(
      () => this.http.get<Item[]>('http://localhost:3000/users')
        .pipe(
          map(items => loadItemsSuccess({ items })),
          catchError(() => of(loadItemsFailed()))
        )
    )
  ));


  saveItems$ = createEffect(() => this.actions$.pipe(
    ofType(saveItem),
    withLatestFrom(this.store.pipe(select(getItemsActive))),
   /*
   switchMap(([action, active]) => {
      return active ?
        of(editItem({ item: { ...action.item, id: active?.id }})) :
        of(addItem({ item: action.item}));
    }),
    */
    map(([action, active]) => {
      return active ?
        editItem({ item: { ...action.item, id: active?.id }}) :
        addItem({ item: action.item});
    })
  ));

  addItem$ = createEffect(() => this.actions$.pipe(
    ofType(addItem),
    concatMap(
      action => this.http.post<Item>('http://localhost:3000/users', action.item)
        .pipe(
          // map(item => addItemSuccess({ item })),
          switchMap(item => [
            addItemSuccess({ item }),
            go({path: 'admin'})
          ]),
          catchError(() => of(addItemFailed()))
        )
    )
  ));

/*
  addItemSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(addItemSuccess),
    mapTo(go({path: 'admin'}))
  ));*/


  editItem$ = createEffect(() => this.actions$.pipe(
    ofType(editItem),
    concatMap(
      action => this.http.patch<Item>('http://localhost:3000/users/' + action.item.id, action.item)
        .pipe(
          map(item => editItemSuccess({ item })),
          catchError(() => of(editItemFailed()))

        )
    )
  ));

  deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(deleteItem),
    mergeMap(
      action => this.http.delete('http://localhost:3000/users/' + action.id)
        .pipe(
          map(() => deleteItemSuccess({ id: action.id })),
          catchError(() => of(deleteItemFailed()))
        )
    )
  ));

  constructor(private actions$: Actions, private store: Store<AppState>, private http: HttpClient) {
  }
}
