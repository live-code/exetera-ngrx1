import { AppState } from '../../../app.module';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UsersFeatureState } from '../items.module';

export const getItemsFeatureState = createFeatureSelector<UsersFeatureState>('users');

export const getItems = createSelector(
  getItemsFeatureState,
  (state: UsersFeatureState) => state.items.list
);

export const getItemsError = createSelector(
  getItemsFeatureState,
  (state: UsersFeatureState) => state.items.error
);

export const getItemsPending = createSelector(
  getItemsFeatureState,
  (state: UsersFeatureState) => state.items.pending
);

export const getItemsActive = createSelector(
  getItemsFeatureState,
  (state: UsersFeatureState) =>  state.items.list.find(item => item.id === state.items.activeId)
);
