import { createAction, props } from '@ngrx/store';
import { Item } from '../model/item';

export const loadItems = createAction(
  '[Item] load',
);

export const loadItemsSuccess = createAction(
  '[Item] load success',
  props<{ items: Item[]}>()
);

export const loadItemsFailed = createAction(
  '[Item] load failed',
);

export const saveItem = createAction(
  '[Item] save',
  props<{ item: Item}>()
);

export const addItem = createAction(
  '[Item] add',
  props<{ item: Item}>()
);
export const addItemSuccess = createAction(
  '[Item] add success',
  props<{ item: Item}>()
);
export const addItemFailed = createAction(
  '[Item] add failed',
);

export const editItem = createAction(
  '[Item] edit',
  props<{ item: Item}>()
);
export const editItemSuccess = createAction(
  '[Item] edit success',
  props<{ item: Item}>()
);
export const editItemFailed = createAction(
  '[Item] edit failed',
);

export const deleteItem = createAction(
  '[Item] delete',
  props<{ id: number }>()
);

export const deleteItemSuccess = createAction(
  '[Item] delete success',
  props<{ id: number }>()
);

export const deleteItemFailed  = createAction(
  '[Item] delete failed',
);


export const setActiveItem = createAction(
  '[Item] set active',
  props<{ id: number }>()
);

export const cleanActiveItem = createAction(
  '[Item] clean active',
);
