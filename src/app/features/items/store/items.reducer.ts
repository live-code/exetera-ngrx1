import { createReducer, on } from '@ngrx/store';
import {
  addItem,
  addItemFailed,
  addItemSuccess, cleanActiveItem,
  deleteItem,
  deleteItemFailed,
  deleteItemSuccess, editItem, editItemFailed, editItemSuccess, loadItems,
  loadItemsFailed,
  loadItemsSuccess, setActiveItem
} from './items.actions';
import { Item } from '../model/item';

export interface ItemsState {
  list: Item[];
  activeId: number;
  error: boolean;
  pending: boolean;
}

const initialState: ItemsState = {
  list: [],
  activeId: null,
  error: false,
  pending: false
};

export const itemsReducer = createReducer(
  initialState,
  on(loadItems, state =>  ({...state, error: false, pending: true })),
  on(loadItemsSuccess, (state, action) => ({ ...state, list: [...action.items], error: false, pending: false }) ),
  on(loadItemsFailed, state => ({ ...state, error: true, pending: false  })),

  on(addItem, state =>  ({...state, error: false, pending: true  })),
  on(addItemSuccess, (state, action) =>  ({ ...state, list: [...state.list, action.item], error: false, pending: false })),
  on(addItemFailed, state => ({ ...state, error: true, pending: false  })),

  on(editItem, state =>  ({...state, error: false, pending: true  })),
  on(editItemSuccess, (state, action) =>  ({
    ...state,
    list: state.list.map(item => item.id === action.item.id ? action.item :item ),
    error: false, pending: false
  })),
  on(editItemFailed, state => ({ ...state, error: true, pending: false  })),

  on(deleteItem, (state, action) => {
    const newActive = state.activeId === action.id ? null : state.activeId;
    return { ...state, error: false, pending: true, activeId: newActive };
  }),
  on(deleteItemSuccess, (state, action) => ({ ...state, list: state.list.filter(item => item.id !== action.id), error: false, pending: false })),
  on(deleteItemFailed, state => ({ ...state, error: true, pending: false  })),

  on(setActiveItem, (state, action) => ({ ...state, activeId: action.id })),
  on(cleanActiveItem, (state) => ({ ...state, activeId: null })),
);
