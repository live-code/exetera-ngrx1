import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { back, go } from '../../core/router/router.actions';

@Component({
  selector: 'app-admin',
  template: `
    <p>
      admin works!
      <button (click)="goSomewhere()">go to items</button>
      <button (click)="backTo()">back </button>
    </p>
  `,
  styles: [
  ]
})
export class AdminComponent implements OnInit {

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
  }

  goSomewhere(): void {
    this.store.dispatch(go({ path: 'items'}))
  }

  backTo(): void {
    this.store.dispatch(back())
  }
}
