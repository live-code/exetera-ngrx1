import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { gotoNextPage, gotoPage, gotoPrevious, searchImages } from './store/pexels-image-search.actions';
import { fromEvent, Observable } from 'rxjs';
import { getPexelsImagePending, getPexelsImages, getPexelsPageIndex } from './store/pexels-image-search.selectors';
import { Photo } from './model/pexels-image-response';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { PexelsImageState } from './pexels-image.module';

@Component({
  selector: 'app-pexels-image',
  template: `
    
    <input type="text" [formControl]="searchInput" required>
    <hr>
  
    <ng-container *appIfLogged="'moderator'">
      <app-paginator
        *ngIf="getPexelsPageIndex$ | async as index"
        [pageIndex]="index"
        (pageChange)="gotoPageHandler($event)"
        (next)="nextHandler()"
        (prev)="prevHandler()"
        max="6"
      ></app-paginator>
    </ng-container>
   
    <div *ngFor="let image of images$ | async">
      <img [src]="image.src.landscape" alt="" width="50">
    </div>
  `,
})
export class PexelsImageComponent implements OnInit {
  searchInput: FormControl = new FormControl('');
  images$: Observable<Photo[]> = this.store.select(getPexelsImages);
  pending$: Observable<boolean> = this.store.select(getPexelsImagePending);
  getPexelsPageIndex$: Observable<number> = this.store.select(getPexelsPageIndex);

  constructor(private store: Store) {
    this.searchInput.valueChanges
      .pipe(
        filter(text => text.length > 2),
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(text => {
        this.store.dispatch(searchImages({ text }))
      });

    this.searchInput.setValue('nature')
  }

  ngOnInit(): void {
  }

  nextHandler(): void {
    this.store.dispatch(gotoNextPage());
  }

  prevHandler(): void {
    this.store.dispatch(gotoPrevious());

  }

  gotoPageHandler(index: number): void {
    this.store.dispatch(gotoPage({ index }))
  }
}
