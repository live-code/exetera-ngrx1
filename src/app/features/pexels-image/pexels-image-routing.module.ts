import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PexelsImageComponent } from './pexels-image.component';

const routes: Routes = [{ path: '', component: PexelsImageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PexelsImageRoutingModule { }
