import { createAction, props } from '@ngrx/store';
import { PexelsImageResponse } from '../model/pexels-image-response';

export const searchImages = createAction(
  '[pexels-image] search',
  props<{ text: string }>()
);


export const searchImagesSuccess = createAction(
  '[pexels-image] search success',
  props<{ response: PexelsImageResponse }>()
);

export const searchImagesFailed = createAction(
  '[pexels-image] search failed',
);


export const gotoPage = createAction(
  '[pexels-image] goto Page',
  props<{ index: number}>()
);
export const gotoNextPage = createAction(
  '[pexels-image] gotoNextPage',
);
export const gotoPrevious = createAction(
  '[pexels-image] gotoPrevPage',
);
