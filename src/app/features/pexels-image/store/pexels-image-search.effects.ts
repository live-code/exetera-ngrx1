import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { gotoNextPage, gotoPage, gotoPrevious, searchImages, searchImagesFailed, searchImagesSuccess } from './pexels-image-search.actions';
import { catchError, concatAll, concatMap, delay, exhaustMap, map, switchMap, switchMapTo, tap, withLatestFrom } from 'rxjs/operators';
import { PexelsVideoResponse } from '../../pexels-video/model/pexels-video-response';
import { PexelsImageResponse } from '../model/pexels-image-response';
import { combineLatest, concat, forkJoin, merge, of, zip } from 'rxjs';
import { Store } from '@ngrx/store';
import {
  getPagePreviousInfo,
  getPexelsImageResponse,
  getPexelsImageText,
  getPexelsNextPage,
  getPexelsPageIndex
} from './pexels-image-search.selectors';


// const TOKEN = '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be'; // mio
const TOKEN = '563492ad6f917000010000014bc088ba26a54c108c2fee86b145c29f'; // vostro

@Injectable()
export class PexelsImageSearchEffects {
  searchImages$ = createEffect(() => this.actions$.pipe(
    ofType(searchImages),
    switchMap(
      action => this.http.get<PexelsImageResponse>(`https://api.pexels.com/v1/search?query=${action.text}&per_page=15&page=1`, {
        headers: { Authorization: TOKEN }
      })
        .pipe(
          map(
            response => searchImagesSuccess({ response }),
            catchError(() => of(searchImagesFailed()))
          )
        )
    )
  ));

  gotoPage$ = createEffect(() => this.actions$.pipe(
    ofType(gotoPage),
    withLatestFrom(this.store.select(getPexelsImageText)),
    switchMap(
      ([action, text]) => this.http.get<PexelsImageResponse>(`https://api.pexels.com/v1/search?query=${text}&per_page=15&page=${action.index}`, {
        headers: { Authorization: TOKEN }
      })
        .pipe(
          map(
            response => searchImagesSuccess({ response }),
            catchError(() => of(searchImagesFailed()))
          )
        )
    )
  ));

  gotoNextPage$ = createEffect(() => this.actions$.pipe(
    ofType(gotoNextPage),
    withLatestFrom(this.store.select(getPexelsNextPage)),
    exhaustMap(
      ([action, nextPage]) => this.http.get<PexelsImageResponse>(nextPage, {
        headers: { Authorization: TOKEN }
      })
        .pipe(
          map(response => searchImagesSuccess({ response }))
        )
    )
  ))

  gotoPrevPage$ = createEffect(() => this.actions$.pipe(
    ofType(gotoPrevious),
    tap(val => console.log(val)),
    switchMap((action) => zip(
      this.store.select(getPexelsImageText),
      this.store.select(getPexelsPageIndex)
      )
    ),
    switchMap(
      ([text, index]) => this.http.get<PexelsImageResponse>(`https://api.pexels.com/v1/search?query=${text}&per_page=15&page=${index-1}`, {
        headers: { Authorization: TOKEN }
      })
        .pipe(
          map(response => searchImagesSuccess({ response }))
        )
    )
  ))

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private store: Store
  ) {
  }
}
