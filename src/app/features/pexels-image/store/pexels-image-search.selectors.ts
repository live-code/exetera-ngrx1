import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsImageState } from '../pexels-image.module';

export const getPexelsImageFeature = createFeatureSelector<PexelsImageState>('pexels-image');

export const getPexelsImagePending = createSelector(
  getPexelsImageFeature,
  state => state.search.pending
);

export const getPexelsImageText = createSelector(
  getPexelsImageFeature,
  state => state.search.text
);

export const getPexelsImageResponse = createSelector(
  getPexelsImageFeature,
  state => state.search.response
);

export const getPexelsImages = createSelector(
  getPexelsImageResponse,
  state => state?.photos
);


export const getPexelsNextPage = createSelector(
  getPexelsImageResponse,
  state => state?.next_page
);


export const getPexelsPageIndex = createSelector(
  getPexelsImageResponse,
  state => state?.page
);

export const getPagePreviousInfo = createSelector(
  getPexelsPageIndex,
  getPexelsImageText,
  (index, text) => ({ index, text })
);

