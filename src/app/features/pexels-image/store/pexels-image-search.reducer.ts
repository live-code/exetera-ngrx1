import { createReducer, on } from '@ngrx/store';
import { PexelsImageResponse } from '../model/pexels-image-response';
import { gotoNextPage, searchImages, searchImagesSuccess } from './pexels-image-search.actions';

export interface PexelsImageSearchState {
  text: string;
  response: PexelsImageResponse;
  pending: boolean;
}

const initialState: PexelsImageSearchState = {
  text: '',
  response: null,
  pending: false,
};

export const pexelsImageSearchReducer = createReducer(
  initialState,
  on(gotoNextPage, (state, action) => ({...state, pending: true})),
  on(searchImages, (state, action) => ({...state, text: action.text, pending: true})),
  on(searchImagesSuccess, (state, action) => ({...state, response: action.response, pending: false})),
)
