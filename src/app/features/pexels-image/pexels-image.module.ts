import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PexelsImageRoutingModule } from './pexels-image-routing.module';
import { PexelsImageComponent } from './pexels-image.component';
import { StoreModule } from '@ngrx/store';
import { pexelsImageSearchReducer, PexelsImageSearchState } from './store/pexels-image-search.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PexelsImageSearchEffects } from './store/pexels-image-search.effects';
import { ReactiveFormsModule } from '@angular/forms';
import { PaginatorComponent } from './paginator/paginator.component';
import { IfloggedModule } from '../../core/auth/iflogged.module';

export interface PexelsImageState {
  search: PexelsImageSearchState
}

const reducers: any = {
  search: pexelsImageSearchReducer
}
@NgModule({
  declarations: [PexelsImageComponent, PaginatorComponent],
  imports: [
    CommonModule,
    IfloggedModule,
    PexelsImageRoutingModule,
    ReactiveFormsModule,
    StoreModule.forFeature('pexels-image', reducers),
    EffectsModule.forFeature([PexelsImageSearchEffects])
  ]
})
export class PexelsImageModule { }
