import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-paginator',
  template: `
    <ul class="pagination">
      <li class="page-item"><a class="page-link" (click)="prev.next()">Previous</a></li>
      
      <li class="page-item"
        *ngFor="let p of [null, null, null, null, null, null]; let i = index">
        <a class="page-link"
           
           (click)="pageChange.emit(pageIndex + i)">{{pageIndex + i}}</a>
      </li>
  
      <li 
        class="page-item"
      ><a class="page-link" (click)="next.emit()">Next</a></li>
    </ul>
  `,
})
export class PaginatorComponent {
  @Input() max: number;
  @Input() pageIndex: number;
  @Output() prev: EventEmitter<void> = new EventEmitter<void>();
  @Output() next: EventEmitter<void> = new EventEmitter<void>();
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();


}
