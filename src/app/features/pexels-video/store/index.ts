import { PexelsVideoSearchState, searchReducer } from './search/pexels-video-search.reducer';
import { PexelsPlayerState, playerReducer } from './player/player.reducer';
import { filterReducer, PexelsFilterState } from './filter/filter.reducer';
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

export interface PexelsVideoState {
  search: PexelsVideoSearchState;
  player: PexelsPlayerState;
  filter: PexelsFilterState;
}

export const reducers: ActionReducerMap<PexelsVideoState> = {
  search: searchReducer,
  player: playerReducer,
  filter: filterReducer
};

export const getPexelsVideoFeature = createFeatureSelector<PexelsVideoState>('pexels-videos');

