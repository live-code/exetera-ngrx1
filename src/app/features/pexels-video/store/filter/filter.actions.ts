import { createAction, props } from '@ngrx/store';

export const filterByMinResolution = createAction(
  '[pexels-video]: filter by min resolution',
  props<{ minResolution: number }>()
)
