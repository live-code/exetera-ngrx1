import { createReducer, on } from '@ngrx/store';
import { filterByMinResolution } from './filter.actions';

export interface PexelsFilterState {
  minResolution: number;
}

export const initialState: PexelsFilterState = {
  minResolution: 0
};

export const filterReducer = createReducer(
  initialState,
  on(filterByMinResolution, (state, action) => ({ ...state, minResolution: action.minResolution}))
)
