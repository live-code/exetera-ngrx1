import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { searchVideos, searchVideosFailed, searchVideosSuccess } from './pexels-video-search.actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { PexelsVideoSearchService } from '../../services/pexels-video.service';
import { of } from 'rxjs';


@Injectable()
export class PexelsVideoSearchEffects {

  searchVideos$ = createEffect(() => this.actions$.pipe(
    ofType(searchVideos),
    switchMap(
      action => this.srv.searchVideos(action.text)
        .pipe(
          map(response => searchVideosSuccess({ items: response.videos })),
          catchError(() => of(searchVideosFailed()))
        )
    )
  ));

  constructor(private actions$: Actions, private srv: PexelsVideoSearchService) {
  }
}
