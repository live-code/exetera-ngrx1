import { createReducer, on } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';
import { searchVideos, searchVideosFailed, searchVideosSuccess } from './pexels-video-search.actions';

export interface PexelsVideoSearchState {
  list: Video[];
  hasError: boolean;
  pending: boolean;
}

const initialState: PexelsVideoSearchState = {
  list: [],
  hasError: false,
  pending: false
};

export const searchReducer = createReducer(
  initialState,
  on(searchVideos, (state, action) => ({ ...state, pending: true })),
  on(searchVideosSuccess, (state, action) => ({ hasError: false, pending: false, list: action.items})),
  on(searchVideosFailed, (state, action) => ({ ...state, hasError: true, pending: false}))
);
