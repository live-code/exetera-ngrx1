import { createAction, props } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';

export const searchVideos = createAction(
  '[pexels-video] search',
  props<{ text: string }>()
);

export const searchVideosSuccess = createAction(
  '[pexels-video] search success',
  props<{ items: Video[] }>()
);

export const searchVideosFailed = createAction(
  '[pexels-video] search failed',
)
