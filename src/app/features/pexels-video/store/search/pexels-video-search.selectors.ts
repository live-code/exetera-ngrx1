import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';
import { getCurrentVideo } from '../player/player.selectors';
import { getPexelsVideoFeature, PexelsVideoState } from '../index';

/*

export const getPexelsVideos = createSelector(
  getPexelsVideoFeature,
  (state: PexelsVideoState) => state.search.list.filter(video => video.width > state.filter.minResolution)
);
*/


export const getPexelsSearchVideos = createSelector(
  getPexelsVideoFeature,
  (state: PexelsVideoState) => state.search.list
);

export const getPexelsSearchFilters = createSelector(
  getPexelsVideoFeature,
  (state: PexelsVideoState) => state.filter
);

export const getPexelsVideos = createSelector(
  getPexelsSearchVideos,
  getPexelsSearchFilters,
  (videos, filters) => videos.filter(video => video.width > filters.minResolution)
);

export const getPexelsVideoSearchPending = createSelector(
  getPexelsVideoFeature,
  (state: PexelsVideoState) => state.search.pending
);

export const getPexelsVideoSearchError = createSelector(
  getPexelsVideoFeature,
  (state: PexelsVideoState) => state.search.hasError
);

export const getPexelsVideoPictures = createSelector(
  getPexelsVideos,
  getCurrentVideo,
  (state: Video[], currentVideo: Video) => state.find(v => v.id === currentVideo?.id)?.video_pictures
    .map(videopicture => videopicture.picture)
);

