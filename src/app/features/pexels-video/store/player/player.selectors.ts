import { createFeatureSelector, createSelector } from '@ngrx/store';
import { getPexelsVideoFeature } from '../index';


export const getCurrentVideo = createSelector(
  getPexelsVideoFeature,
  state => state.player.currentVideo
)

export const getCurrentVideoUrl = createSelector(
  getCurrentVideo,
  state => state?.video_files[0].link
)
