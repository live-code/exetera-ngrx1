import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { searchVideos } from './store/search/pexels-video-search.actions';
import { Observable } from 'rxjs';
import { Video } from './model/pexels-video-response';
import {
  getPexelsVideoPictures,
  getPexelsVideos,
  getPexelsVideoSearchError,
  getPexelsVideoSearchPending
} from './store/search/pexels-video-search.selectors';
import { showVideo } from './store/player/player.actions';
import { getCurrentVideoUrl } from './store/player/player.selectors';
import { filterByMinResolution } from './store/filter/filter.actions';

@Component({
  selector: 'app-pexels-video',
  template: `
    <hr>
    <div style="width: 100%; max-width: 400px; margin: 0 auto">
      
      <!--Search Form-->
      <div>
        <input type="text" (keydown.enter)="searchVideoHandler($event)">
        <div *ngIf="pending$ | async">loading...</div>
      </div>
      
      <!--Video player-->
      <video  *ngIf="currentVideoUrl$ | async as url"
              width="100%" [src]="url" controls>
        Your browser does not support the video tag.
      </video>

      <!--VIdeo PIctures-->
      <div style="display: flex; width: 100%; overflow: auto">
        <div *ngFor="let picture of videoPictures$ | async">
          <img [src]="picture" alt="" width="50">
        </div>
      </div>
      
      <!--Video Search Result-->
      <hr>
      <select (change)="filterByResolution($event)">
        <option>0</option>
        <option>2000</option>
        <option>3000</option>
        <option>4000</option>    
      </select>
      <div style="display: flex; width: 100%; overflow: auto">
        <div 
          *ngFor="let video of videos$ | async" 
          (click)="showPreview(video)"
          style="text-align: center"
        >
          <img [src]="video.image" width="100">
          <div style="font-size: 10px">{{video.width}} * {{video.height}}</div>
        </div>
      </div>
    </div>
  `,
})
export class PexelsVideoComponent {
  videos$: Observable<Video[]> = this.store.pipe(select(getPexelsVideos));
  pending$: Observable<boolean> = this.store.pipe(select(getPexelsVideoSearchPending));
  currentVideoUrl$: Observable<string> = this.store.pipe(select(getCurrentVideoUrl));
  videoPictures$: Observable<string[]> = this.store.pipe(select(getPexelsVideoPictures));

  constructor(private store: Store<AppState>) {
    store.dispatch(searchVideos({ text: 'girls' }));
  }

  showPreview(video: Video): void {
    this.store.dispatch(showVideo({ video }));
  }

  searchVideoHandler(event: KeyboardEvent): void {
    this.store.dispatch(
      searchVideos({
        text: (event.currentTarget as HTMLInputElement).value
      })
    );
  }

  filterByResolution(event: Event): void {
    this.store.dispatch(filterByMinResolution({
      minResolution: +(event.currentTarget as HTMLSelectElement).value
    }));
  }
}
