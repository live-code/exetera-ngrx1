import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PexelsVideoRoutingModule } from './pexels-video-routing.module';
import { PexelsVideoComponent } from './pexels-video.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PexelsVideoSearchEffects } from './store/search/pexels-video-search.effects';
import { PexelsVideoSearchService } from './services/pexels-video.service';
import { FormsModule } from '@angular/forms';
import { reducers } from './store';

@NgModule({
  declarations: [PexelsVideoComponent],
  imports: [
    CommonModule,
    FormsModule,
    PexelsVideoRoutingModule,
    StoreModule.forFeature('pexels-videos', reducers),
    EffectsModule.forFeature([PexelsVideoSearchEffects])
  ],
  providers: [
    PexelsVideoSearchService
  ]
})
export class PexelsVideoModule { }
