import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { login } from '../../core/auth/store/auth.actions';
import { Observable } from 'rxjs';
import { getAuthError } from '../../core/auth/store/auth.selectors';

@Component({
  selector: 'app-login',
  template: `
    <div class="alert alert-danger" *ngIf="getAuthError$ | async">
      errore login
    </div>
    
    <form [formGroup]="form" (submit)="loginHandler()">
      <input type="text" formControlName="username">
      <input type="text" formControlName="password">
      <button type="submit" [disabled]="form.invalid">LOGIN</button>
    </form>
  `,
})
export class LoginComponent {
  getAuthError$: Observable<boolean> = this.store.select(getAuthError)
  form: FormGroup;

  constructor(private fb: FormBuilder, private store: Store) {
    this.form = fb.group({
      username: [
        '',
        Validators.compose([
          Validators.required, Validators.minLength(3)
        ])
      ],
      password: ''
    })
  }

  loginHandler(): void {
    this.store.dispatch(
      login({
        username: this.form.value.username,
        password: this.form.value.password,
      })
    )
  }
}
