import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { back, go } from './router.actions';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class RouterEffects {
  go$ = createEffect(() => this.actions$.pipe(
    ofType(go),
    tap(action => this.router.navigateByUrl(action.path))
  ), { dispatch: false })

 back$ = createEffect(() => this.actions$.pipe(
    ofType(back),
    tap(action => this.location.back())
  ), { dispatch: false })

  constructor(private actions$: Actions, private router: Router, private location: Location) {}
}
