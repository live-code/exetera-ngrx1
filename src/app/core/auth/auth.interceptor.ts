import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { Store } from '@ngrx/store';
import { getAuthToken } from './store/auth.selectors';
import { catchError, mergeMap, take } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { go } from '../router/router.actions';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private store: Store) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select(getAuthToken)
      .pipe(
        take(1),
        mergeMap(token => {
          console.log(token);
          const authReq = !!token && req.url.includes('/users') ? req.clone({
            setHeaders: { Authorization: 'Bearer ' + token }
          }) : req;
          return next.handle(authReq)
            .pipe(
              catchError(err => {
                if (err instanceof HttpErrorResponse) {
                  switch (err.status) {
                    case 401:
                    case 404:
                    default:
                      this.store.dispatch(go({ path: 'login'}))
                      break;
                  }
                }
                return throwError(null)
              })
            )
        })
      );
  }
}
