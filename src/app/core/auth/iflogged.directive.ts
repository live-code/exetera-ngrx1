import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { getAuthRole, getIsLogged } from './store/auth.selectors';
import { distinctUntilChanged, filter, takeUntil, tap, withLatestFrom } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';

@Directive({
  selector: '[appIfLogged]'
})
export class IfloggedDirective implements OnDestroy, OnInit {
  @Input() appIfLogged: string;
  sub: Subscription;
  subject: Subject<void> = new Subject<void>();

  constructor(
    private template: TemplateRef<any>,
    private view: ViewContainerRef,
    private store: Store
  ) {

  }

  ngOnInit(): void {
    console.log('appIflogged')
    this.sub = this.store.select(getIsLogged)
      .pipe(
        takeUntil(this.subject),
        tap(() => this.view.clear()),
        withLatestFrom(this.store.select(getAuthRole)),
        filter(([isLogged, role]) => {
          console.log('role', role, this.appIfLogged)
          return isLogged && role === this.appIfLogged;
        }),
        // distinctUntilChanged(),
      )
      .subscribe(isLogged => {
        console.log('appIflogged',  isLogged)
        this.view.createEmbeddedView(this.template);
      });
  }
  ngOnDestroy(): void {
    this.subject.next();
    this.subject.complete();
  }
}
