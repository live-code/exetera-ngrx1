import { NgModule } from '@angular/core';
import { IfloggedDirective } from './iflogged.directive';

@NgModule({
  declarations: [IfloggedDirective],
  exports: [IfloggedDirective],
})
export class IfloggedModule {

}
