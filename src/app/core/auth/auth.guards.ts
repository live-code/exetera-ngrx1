import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { getIsLogged } from './store/auth.selectors';
import { tap } from 'rxjs/operators';
import { go } from '../router/router.actions';

@Injectable({ providedIn: 'root' })
export class AuthGuards implements CanActivate {
  constructor(private store: Store) {
  }
  canActivate(): Observable<boolean>{
    return this.store.select(getIsLogged)
      .pipe(
        tap(isLogged => {
          if (!isLogged) {
            this.store.dispatch(go({ path: 'login'}))
          }
        })
      );
  }
}
