import { AppState } from '../../../app.module';
import { createSelector } from '@ngrx/store';


export const getAuth = (state: AppState) => state.authentication.auth;
export const getIsLogged = (state: AppState) => !!state.authentication.auth;

export const getAuthToken = createSelector(
  getAuth,
  (state) => state?.token
);

export const getAuthDisplayName = (state: AppState) => state.authentication.auth.displayName;
export const getAuthRole = (state: AppState) => state.authentication.auth?.role;
export const getAuthError = (state: AppState) => state.authentication.error;
