import { createAction, props } from '@ngrx/store';
import { Auth } from '../auth';

export const login = createAction(
  '[auth] login',
  props<{ username: string, password: string }>()
);

export const syncWithLocalSTorage = createAction(
  '[auth] sync with localStorage',
  props<{ auth: Auth }>()
);

export const loginSuccess = createAction(
  '[auth] login success',
  props<{ auth: Auth }>()
);

export const loginFailed = createAction('[auth] login failed');
export const logout = createAction('[auth] logout');
