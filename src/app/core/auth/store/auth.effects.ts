import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as AuthActions from './auth.actions';
import { catchError, exhaustMap, filter, map, mapTo, tap } from 'rxjs/operators';
import { Auth } from '../auth';
import { of } from 'rxjs';
import { go } from '../../router/router.actions';

@Injectable()
export class AuthEffects {
  init$ = createEffect(() => this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    mapTo(JSON.parse(localStorage.getItem('auth')) as Auth),
    filter(auth => !!auth),
    map(auth => AuthActions.syncWithLocalSTorage({ auth }))
  ));

  login$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.login),
    exhaustMap(
      action => {
        const params = new HttpParams()
          .set('username', action.username)
          .set('password', action.password);
        return this.http.get<Auth>('http://localhost:3000/login', { params })
          .pipe(
            map(auth => AuthActions.loginSuccess({ auth }) ),
            catchError(() => of(AuthActions.loginFailed()))
          )
      }
    )
  ));

  loginSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.loginSuccess),
    tap(action => localStorage.setItem('auth', JSON.stringify(action.auth))),
    mapTo(go({ path: 'items'}))
  ));

  logout$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.logout),
    tap(action => localStorage.removeItem('auth')),
    mapTo(go({ path: 'login'}))
  ));

  constructor(private actions$: Actions, private http: HttpClient) {
  }
}
