import { createReducer, on } from '@ngrx/store';
import { Auth } from '../auth';
import * as AuthActions from './auth.actions';

export interface AuthState {
  auth: Auth;
  error: boolean;
}
const initialState: AuthState = {
  auth: null,
  error: false
};

export const authReducer = createReducer(
  initialState,
  on(AuthActions.syncWithLocalSTorage, (state, action) => ({ ...state, auth: action.auth, error: false })),
  on(AuthActions.loginSuccess, (state, action) => ({ ...state, auth: action.auth, error: false })),
  on(AuthActions.loginFailed, (state, action) => ({ ...state, error: true })),
  on(AuthActions.logout, (state, action) => ({ ...state, auth: null})),

);
