import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionReducer, ActionReducerMap, MetaReducer, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { EffectsModule, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { Auth } from './core/auth/auth';
import { routerReducer, RouterReducerState, RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { RouterEffects } from './core/router/router.effects';
import { PaginatorComponent } from './features/pexels-image/paginator/paginator.component';
import { authReducer, AuthState } from './core/auth/store/auth.reducer';
import { AuthEffects } from './core/auth/store/auth.effects';
import { AuthInterceptor } from './core/auth/auth.interceptor';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { IfloggedDirective } from './core/auth/iflogged.directive';
import { IfloggedModule } from './core/auth/iflogged.module';

export interface AppState {
  authentication: AuthState;
  router: RouterReducerState;
}
export const reducers: ActionReducerMap<AppState> = {
  authentication: authReducer,
  router: routerReducer
};

// console.log all actions
export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state: AppState, action) => {
    console.log('state', state);
    console.log('action', action);

    let s: any = {...state};
    if (action.type === ROOT_EFFECTS_INIT) {
      s = { ...state, authentication: { token: '123'} }
    }
    return reducer(s, action);
  };
}

export const metaReducers: MetaReducer<any>[] = [debug];


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    IfloggedModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    // StoreModule.forRoot(reducers, { metaReducers }),
    StoreModule.forRoot(reducers ),
    StoreDevtoolsModule.instrument({
      maxAge: 20
    }),
    EffectsModule.forRoot([RouterEffects, AuthEffects]),
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Minimal
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: AuthInterceptor
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private router: Router) {
    this.router.events
      .pipe(
        filter(navigation => navigation instanceof NavigationEnd)
      )
      .subscribe(console.log)
  }
}
